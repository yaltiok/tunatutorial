package com.etxample.grumwelllap.listtutorial;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.etxample.grumwelllap.listtutorial.firebaseUI.FirebaseViewHolder;
import com.etxample.grumwelllap.listtutorial.firebaseUI.IItemClickListener;
import com.etxample.grumwelllap.listtutorial.repository.PersonRepository;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Person> persons = new ArrayList<>();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef = database.getReference();
    private DatabaseReference personRef = database.getReference("Person");
    private String selectedKey;
    private RecyclerView recyclerView;
    private Button addBtn;
    Person selectedPerson;

    FirebaseRecyclerOptions<Person> options;
    FirebaseRecyclerAdapter<Person, FirebaseViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //myRef = database.getReference("Person");
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PersonDetailActivity.class);
                startActivity(intent);
            }
        });
        fillArrayList();
    }


    public void initialize() {


        options =
                new FirebaseRecyclerOptions.Builder<Person>()
                        .setQuery(personRef, Person.class)
                        .build();

        adapter =
                new FirebaseRecyclerAdapter<Person, FirebaseViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull FirebaseViewHolder holder, int position, @NonNull final Person model) {
                        holder.personNameLabel.setText(String.valueOf(model.getName()));
                        holder.personAgeLabel.setText(String.valueOf(model.getAge()));
                        if (model.isSex()) {
                            holder.personSexLabel.setText("E");
                        } else {
                            holder.personSexLabel.setText("K");
                        }

                        holder.setiItemClickListener(new IItemClickListener() {
                            @Override
                            public void OnClick(View view, int position) {
                                selectedPerson = model;
                                selectedKey = getSnapshots().getSnapshot(position).getKey();
                                Intent intent = new Intent(MainActivity.this, PersonDetailActivity.class);
                                intent.putExtra(RuntimeConstant.KEY_PERSON_DETAIL, persons.get(position));
                                intent.putExtra(RuntimeConstant.PERSON_ID, selectedKey);
                                startActivity(intent);
                            }

                            @Override
                            public boolean OnLongClick(View v, int position) {
                                selectedPerson = model;
                                selectedKey = getSnapshots().getSnapshot(position).getKey();

                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle(selectedPerson.getName());
                                builder.setMessage("Kişiyi silmek istiyor musunuz?");
                                builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });

                                builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        PersonRepository repo = new PersonRepository();
                                        repo.remove(selectedKey);
                                    }
                                });

                                builder.show();
                                Log.e("TAGGgg", "OnLONgClick: " + selectedKey);
                                return true;
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public FirebaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View itemView = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_view_list, viewGroup, false);
                        return new FirebaseViewHolder(itemView);
                    }
                };

        adapter.startListening();

        recyclerView.setAdapter(adapter);
        //ListAdapter listViewAdapter = new ListAdapter(MainActivity.this, persons);
        //listView.setAdapter(listViewAdapter);
        //listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        //    @Override
        //    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //        Intent intent = new Intent(MainActivity.this, PersonDetailActivity.class);
        //        intent.putExtra(RuntimeConstant.KEY_PERSON_DETAIL, persons.get(position));
        //        startActivity(intent);
        //    }
        //});
    }


    private void fillArrayList() {

        PersonRepository personRepository = new PersonRepository();
        persons = personRepository.list();
        initialize();


        //personRef.addValueEventListener(new ValueEventListener() {
        //    @Override
        //    public void onDataChange(DataSnapshot dataSnapshot) {
        //        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
        //            persons.add(snapshot.getValue(Person.class));
        //        }
        //        initialize();
        //    }
        //    @Override
        //    public void onCancelled(DatabaseError databaseError) {
        //        System.out.println("The read failed: " + databaseError.getCode());
        //    }
        //});
        //  for (int index = 0; index < 20; index++) {
        //      Person person = new Person("Mr. Android " + index + "Nowhere",index+10,true,"","");
        //      persons.add(person);
        //  }
    }
}

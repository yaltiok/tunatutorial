package com.etxample.grumwelllap.listtutorial.repository;

import com.etxample.grumwelllap.listtutorial.Person;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PersonRepository {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef = database.getReference();
    private DatabaseReference personRef = database.getReference("Person");
    private ArrayList<Person> personList;

    public ArrayList<Person> list(){
        if (personList == null){
            personList = new ArrayList<>();
        }
        personRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    personList.add(snapshot.getValue(Person.class));

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        return personList;
    }

    public void add(Person person){
        personRef.push().setValue(person);
    }

    public void remove(String id){
        personRef.child(id).removeValue();
    }

    public void find(Person person){

    }

    public void update(Person person,String id){
        personRef.child(id).setValue(person);
    }
}

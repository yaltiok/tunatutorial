package com.etxample.grumwelllap.listtutorial.firebaseUI;

import android.view.View;

public interface IItemClickListener {
    void OnClick(View view,int position);
    boolean OnLongClick(View v,int position);
}

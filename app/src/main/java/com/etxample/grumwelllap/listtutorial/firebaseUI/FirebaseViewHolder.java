package com.etxample.grumwelllap.listtutorial.firebaseUI;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.etxample.grumwelllap.listtutorial.R;

public class FirebaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    IItemClickListener iItemClickListener;

    public void setiItemClickListener(IItemClickListener iItemClickListener) {
        this.iItemClickListener = iItemClickListener;
    }

    public TextView personNameLabel;
    public TextView personAgeLabel;
    public TextView personSexLabel;

    public FirebaseViewHolder(@NonNull View itemView) {
        super(itemView);


        personNameLabel = (TextView) itemView.findViewById(R.id.nameTxt);
        personAgeLabel = (TextView) itemView.findViewById(R.id.ageTxt);
        personSexLabel = (TextView) itemView.findViewById(R.id.sexTxt);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View v) {
        iItemClickListener.OnClick(v,getAdapterPosition());
    }

    @Override
    public boolean onLongClick(View v) {
        return iItemClickListener.OnLongClick(v,getAdapterPosition());
    }
}

package com.etxample.grumwelllap.listtutorial;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends ArrayAdapter<Person> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<Person> persons;

    public ListAdapter(Context context, ArrayList<Person> persons) {
        super(context, 0, persons);
        this.context = context;
        this.persons = persons;
        inflater = LayoutInflater.from(context);
    }

    private static class ViewHolder{
        TextView personNameLabel;
        TextView personAgeLabel;
        TextView personSexLabel;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.item_view_list, null);

            holder = new ViewHolder();
            holder.personNameLabel = (TextView) convertView.findViewById(R.id.nameTxt);
            holder.personAgeLabel = (TextView) convertView.findViewById(R.id.ageTxt);
            holder.personSexLabel = (TextView) convertView.findViewById(R.id.sexTxt);
            holder.personSexLabel.setBackgroundColor(55555);
            convertView.setTag(holder);

        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        Person person = persons.get(position);
        if(person != null){
            holder.personNameLabel.setText(person.getName());
            holder.personAgeLabel.setText(Integer.toString(person.getAge()));
            if (person.isSex()){
                holder.personSexLabel.setText("E");
            }else{
                holder.personSexLabel.setText("K");
            }


        }

        return convertView;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Person getItem(int position) {
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return persons.get(position).hashCode();
    }

}
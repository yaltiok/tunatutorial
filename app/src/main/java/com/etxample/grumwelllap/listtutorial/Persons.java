package com.etxample.grumwelllap.listtutorial;

import java.util.ArrayList;

public class Persons {

    ArrayList<Person> personArrayList;

    public ArrayList<Person> getPersonArrayList() {
        return personArrayList;
    }

    public void setPersonArrayList(ArrayList<Person> personArrayList) {
        this.personArrayList = personArrayList;
    }
}

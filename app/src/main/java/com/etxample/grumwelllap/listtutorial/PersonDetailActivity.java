package com.etxample.grumwelllap.listtutorial;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.etxample.grumwelllap.listtutorial.repository.PersonRepository;

import java.io.Console;

public class PersonDetailActivity extends AppCompatActivity {

    private EditText nameTxt;
    private EditText ageTxt;
    private EditText addressTxt;
    private EditText phoneEdt;
    private Spinner spnr;
    private Person person;
    private Button addBtn;
    private String selectedKey = "";
    private boolean isSex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);
        nameTxt = findViewById(R.id.etName);
        ageTxt = findViewById(R.id.etAge);
        spnr = findViewById(R.id.sexSpinner);
        addBtn = findViewById(R.id.addButton);
        addressTxt = findViewById(R.id.adrsEt);
        phoneEdt = findViewById(R.id.phoneEdt);
        checkExtras();
    }


    private void checkExtras() {
        try {
            selectedKey = getIntent().getExtras().getString(RuntimeConstant.PERSON_ID);
            if (!selectedKey.isEmpty()) {
                addBtn.setText("UPDATE");
            }
            Log.e("Check", "checkExtras: ");

            person = (Person) getIntent().getExtras().getSerializable(RuntimeConstant.KEY_PERSON_DETAIL);
            nameTxt.setText(person.getName());
            ageTxt.setText(Integer.toString(person.getAge()));
            phoneEdt.setText(person.getPhoneNumber());
            addressTxt.setText(person.getAddress());
            if (person.isSex()) {
                spnr.setSelection(0);
            } else {
                spnr.setSelection(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        submitClickListener();

    }

    private void add() {
        PersonRepository personRepository = new PersonRepository();
        personRepository.add(person);
    }

    private void update() {
        PersonRepository personRepository = new PersonRepository();
        personRepository.update(person, selectedKey);
    }

    public boolean checkFields() {
        return !nameTxt.getText().toString().isEmpty()
                && !phoneEdt.getText().toString().isEmpty()
                && !addressTxt.getText().toString().isEmpty()
                && !ageTxt.getText().toString().isEmpty();
    }

    private void submitClickListener() {
        if (selectedKey.isEmpty()) {
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkFields()) {
                        Toast.makeText(PersonDetailActivity.this, "Her yeri doldur baba", Toast.LENGTH_LONG).show();
                    } else {
                        if (spnr.getSelectedItem().toString().equals("Erkek")) {
                            isSex = true;
                        } else {
                            isSex = false;
                        }
                        person = new Person(nameTxt.getText().toString(), Integer.parseInt(ageTxt.getText().toString()), isSex, addressTxt.getText().toString(), phoneEdt.getText().toString());
                        add();
                        Intent intent = new Intent(PersonDetailActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                }
            });
        } else {
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkFields()) {
                        Toast.makeText(PersonDetailActivity.this, "Her yeri doldur baba", Toast.LENGTH_LONG).show();
                    } else {
                        if (spnr.getSelectedItem().toString().equals("Erkek")) {
                            isSex = true;
                        } else {
                            isSex = false;
                        }
                        person = new Person(nameTxt.getText().toString(), Integer.parseInt(ageTxt.getText().toString()), isSex, addressTxt.getText().toString(), phoneEdt.getText().toString());
                        update();
                        Intent intent = new Intent(PersonDetailActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }
}
